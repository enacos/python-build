ARG BASE_IMAGE=python:3.10-slim-bookworm

############################################################################
# First stage
#
FROM $BASE_IMAGE AS builder
LABEL stage=builder
RUN apt-get update -y && apt-get install -y \
        apt-transport-https \
        build-essential \
        ca-certificates \
        curl \
        git \
        gnupg2 \
        jq \
        make \
        python3-dev \
        software-properties-common \
        virtualenv \
        zip

# Node / NPM (LTS) repo
RUN mkdir -p /etc/apt/keyrings && curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

# Install
RUN apt-get update && \
    apt-get install -y \
        nodejs npm

# install poetry
RUN curl -sSL https://install.python-poetry.org | python3 -

# clean up
RUN apt-get autoremove --purge -y && apt-get clean && rm -rf /var/lib/apt/lists/* && rm -rf /etc/apt/sources.list.d/*

############################################################################
# Second stage
#

FROM $BASE_IMAGE AS build-image
COPY --from=builder / ./
ENV PATH="/root/.local/bin:/home/usr/bin:${PATH}"
WORKDIR /home
CMD ["/bin/bash"]