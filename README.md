# Python build images using Docker-in-Docker for Gitlab shared runners 

![build status](https://gitlab.com/roemer/python-build/badges/master/pipeline.svg)

These are custom Docker images for building Docker images on shared Gitlab runners. They are based on the official `Python:3.8-slim-bookworm` image.


## Python versions

The following images are available:

| Tags   | Id                                             | Description                                                     |
|--------|------------------------------------------------|-----------------------------------------------------------------|
| `3.8`  | `registry.gitlab.com/roemer/python-build:3.8`  | The latest [Python 3.8 image](https://hub.docker.com/_/python)  |
| `3.9`  | `registry.gitlab.com/roemer/python-build:3.9`  | The latest [Python 3.9 image](https://hub.docker.com/_/python)  |
| `3.10` | `registry.gitlab.com/roemer/python-build:3.10` | The latest [Python 3.10 image](https://hub.docker.com/_/python) |
| `3.11` | `registry.gitlab.com/roemer/python-build:3.11` | The latest [Python 3.11 image](https://hub.docker.com/_/python) |
| `3.12` | `registry.gitlab.com/roemer/python-build:3.12` | The latest [Python 3.12 image](https://hub.docker.com/_/python) |

All images are build from Debian Bookworm (`slim-bookworm`).

## Image versions
All versions are available in the following "sub versions" to keep the main build image as small as possible.

| Tags          | Description                                                                                    |
|---------------|------------------------------------------------------------------------------------------------|
| `3.10`        | The **build** image: contains Python, Poetry, and build essentails                             |
| `3.10-deploy` | Additionally, this **deployment** image contains Docker, Kubectl, and Helm                     |
| `3.10-doc`    | Additionally, this **doccumentation** image contains GraphViz, PlantUml, and the headless JRE  |

## Container registry

The images can be found on `registry.gitlab.com/roemer/python-build`; see https://gitlab.com/roemer/python-build/container_registry

## Using Docker-in-Docker (DinD) for builds

The image uses "Docker-in-Docker" approach as described in https://docs.gitlab.com/ee/ci/docker/using_docker_build.html,
but with custom images for the build runner. 

The following environment settings are essential:

- `DOCKER_HOST: tcp://docker:2376`    use the Docker TLS port
- `DOCKER_TLS_VERIFY: 1`    use TLS
- `DOCKER_TLS_CERTDIR: "/certs"` generate certificates in this directory 
- `DOCKER_CERT_PATH: "/certs/client"` use the certificates in this directory

## Usage in Gitlab CI/CD  

Here's a demo `.gitlab-ci.yml` file: 
 
```yaml
image: registry.gitlab.com/roemer/python-build:3.8

stages:
  - test

services:
  - name: docker:24.0-dind

variables:
  # Docker TLS configuration
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_VERIFY: 1
  DOCKER_TLS_CERTDIR: "/certs"
  DOCKER_CERT_PATH: "/certs/client"
  DOCKER_DRIVER: overlay2

before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

test:
  stage: test
  script:
  - docker build --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA --tag $CI_REGISTRY_IMAGE:latest --no-cache .
```  
Please note the Docker variables!