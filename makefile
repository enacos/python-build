BASE_IMAGE=python:3.12-slim-bookworm
IMAGE_NAME=python-build

build:
	docker build --pull --tag $(IMAGE_NAME):latest --tag $(IMAGE_NAME):build --build-arg BASE_IMAGE=$(BASE_IMAGE) --file=dockerfile .
	docker build --tag $(IMAGE_NAME):deploy --build-arg BASE_IMAGE=$(IMAGE_NAME):build --file=dockerfile-deploy .
	docker build --tag $(IMAGE_NAME):doc --build-arg BASE_IMAGE=$(IMAGE_NAME):build --file=dockerfile-doc .

save:
	docker save $(IMAGE_NAME):latest | gzip > $(IMAGE_NAME).latest.tar.gz
	docker save $(IMAGE_NAME):deploy | gzip > $(IMAGE_NAME).deploy.tar.gz
	docker save $(IMAGE_NAME):doc | gzip > $(IMAGE_NAME).doc.tar.gz &

build-all:
	$(MAKE) -e BASE_IMAGE=python:3.8-slim-bookworm build
	$(MAKE) -e BASE_IMAGE=python:3.9-slim-bookworm build
	$(MAKE) -e BASE_IMAGE=python:3.10-slim-bookworm build
	$(MAKE) -e BASE_IMAGE=python:3.11-slim-bookworm build
	$(MAKE) -e BASE_IMAGE=python:3.12-slim-bookworm build

all:
	$(MAKE) -e BASE_IMAGE=python:3.8-slim-bookworm build save
	$(MAKE) -e BASE_IMAGE=python:3.9-slim-bookworm build save
	$(MAKE) -e BASE_IMAGE=python:3.10-slim-bookworm build save
	$(MAKE) -e BASE_IMAGE=python:3.11-slim-bookworm build save
	$(MAKE) -e BASE_IMAGE=python:3.12-slim-bookworm build save

run:
	docker run -it python-build:latest

run-deploy:
	docker run -it python-build:deploy

run-doc:
	docker run -it python-build:doc
